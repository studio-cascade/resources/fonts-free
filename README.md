

# Free fonts



## Hanken Grotestk (Alfredo Marco Pradil, Hanken Design Co.)

```
<link rel="stylesheet" type="text/css" href="fonts/hanken-grotesk/stylesheet.css">

font-family: 'Hanken Grotesk';
```

https://fonts.google.com/specimen/Hanken+Grotesk

Folder crate with https://transfonter.org/




## Inter

```
 <link rel="stylesheet" type="text/css" href="fonts/inter/stylesheet.css">

font-family: InterVariable;
font-weight: [100-900]

font-family: "Inter"; 
font-weight: [100-900]

font-family: "InterDisplay";
font-weight: [100-900]
```

https://rsms.me/inter/

Downloaded folder






### Poppins

```
<link rel="stylesheet" type="text/css" href="fonts/poppins/stylesheet.css">

font-family: 'Poppins';
font-weight: [100-900]

```

https://fonts.google.com/specimen/Poppins

Folder crate with https://transfonter.org/






### Satoshi

```
<link rel="stylesheet" type="text/css" href="fonts/poppins/stylesheet.css">

font-family: 'Satoshi';
font-weight: [300-400-500-700-900];

font-family: 'Satoshi';
font-weight: [300-900];
```

https://www.fontshare.com/fonts/satoshi

Folder crate with https://transfonter.org/







----



## Ajouter une font avec git



Être dans le dossier de puis le terminal.

Avant d’ajouter quoi que ce soit (notamment au readme), récupérer les derniers changements:

```
git pull
```

Ajouter le dossier de typographie et remplir les informations dans le README.md, puis:

```
gaa
gc -m "add [nom de la font]"
git push
```

